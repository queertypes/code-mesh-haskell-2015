# 0.2.0.0 (Dec. 18, 2015)

* Various updates to make the materials easier to use w/o a presenter

# 0.1.0.0 (Sep. 20, 2015)

* Initial commit
