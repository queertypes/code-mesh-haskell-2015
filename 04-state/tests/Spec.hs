module Main where

import Data.Aeson
import Test.Tasty
import Test.Tasty.HUnit

import API.Models.Common
import API.Models.User

-- (decode . encode) R == R

jsonTests :: TestTree
jsonTests =
  testGroup "json" [ testCase "round trip" $ (decode . encode) Red @=? (Just Red)
                   ]

main :: IO ()
main = defaultMain jsonTests
