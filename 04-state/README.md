# Code Mesh: Start

This tutorial section covers:

* JSON handling
* Registering a new route
* Echoing JSON
* In-memory state using Haskell's STM facilities
* Simple authentication
* Logging
* Error reporting
