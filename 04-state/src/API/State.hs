module API.State where

import Control.Concurrent.STM.TVar
import Data.Map
import API.Models.Common
import API.Models.User
import API.Logging

type UserRegistry = Map (Username,Pass) User

data State
  = State { users :: TVar UserRegistry
          , logger :: Log
          }
