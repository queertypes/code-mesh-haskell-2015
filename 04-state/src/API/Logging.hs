module API.Logging (
  -- * Initialize
  mkLog,

  -- * Context, Types
  Log,

  -- * Logging
  fatal,
  err,
  warn,
  notice,
  info,
  debug,
  flush,

  -- * Convenience
  (<>)
) where

import Prelude hiding (log)

import Data.Monoid
import Data.Time.Clock (getCurrentTime)
import Data.Time.Format
import Network.HostName
import System.Posix.Process (getProcessID)
import System.Log.FastLogger

type Log = LoggerSet

mkLog :: IO Log
mkLog = newStdoutLoggerSet defaultBufSize

-- 2015-10-20T10:10:10:parametric:codemesh-workshop:7825:User codemesh_cat registered
log :: ToLogStr m => LogStr -> Log -> m -> IO ()
log lv l m = do
  now <- getCurrentTime
  hname <- fmap toLogStr getHostName
  pid <- (toLogStr . show) <$> getProcessID
  let locale = defaultTimeLocale
  let tForm = "%Y-%m-%dT%H:%M:%SZ"
  let projectName = "codemesh-workshop"
  let timestamp = toLogStr (formatTime locale tForm now)
  pushLogStrLn l $ lv
                 <> ":" <> timestamp
                 <> ":" <> hname
                 <> ":" <> projectName
                 <> ":" <> pid
                 <> ":" <> toLogStr m

fatal :: ToLogStr m => Log -> m -> IO ()
fatal = log "FATAL"

err :: ToLogStr m => Log -> m -> IO ()
err = log "ERROR"

warn :: ToLogStr m => Log -> m -> IO ()
warn = log "WARNING"

notice :: ToLogStr m => Log -> m -> IO ()
notice = log "NOITCE"

info :: ToLogStr m => Log -> m -> IO ()
info = log "INFO"

debug :: ToLogStr m => Log -> m -> IO ()
debug = log "DEBUG"

flush :: Log -> IO ()
flush = flushLogStr
