module API.Routes (
  routes
) where

import API.Controllers.Users
import API.State

import Web.Spock.Safe

routes :: State -> SpockT IO ()
routes st = do
  get "hello" (text "hi there!")
  post ("echo" <//> "users") echoUser

  post "register" (registerUser (logger st) (users st))
  post "login" (login (users st))

-- Authentication: Basic user:pass
