module API.Models.Common where

import Data.Text

newtype Pass     = Pass Text deriving (Eq, Ord)
newtype Username = Username Text deriving (Eq, Ord)
