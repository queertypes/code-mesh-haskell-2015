module API.Models.User where

import Data.Aeson
import Data.UUID

import API.Models.Common

data User
  = User { pid :: UUID
         , username :: Username
         , faveColor :: Color
         } deriving (Eq)

data NewUser
  = NewUser Username Pass Color

data Color
  = Red
  | Green
  | Blue
    deriving (Show, Eq)

instance ToJSON Color where
  toJSON Red   = "red"
  toJSON Green = "green"
  toJSON Blue  = "blue"

instance FromJSON Color where
  parseJSON (String "red")   = pure Red
  parseJSON (String "blue")  = pure Blue
  parseJSON (String "green") = pure Green
  parseJSON _ = mempty

instance FromJSON NewUser where
  parseJSON (Object o) =
    NewUser <$> (Username <$> (o .: "username"))
            <*> (Pass <$> (o .: "password"))
            <*> (o .: "color")
  parseJSON _ = mempty

instance ToJSON User where
  toJSON (User p (Username u) c) =
    object [ "id" .= toText p
           , "username" .= toJSON u
           , "color" .= toJSON c
           ]

instance FromJSON User where
  parseJSON (Object o) =
    User <$> (o .: "id" >>= (maybe mempty pure . fromText))
         <*> (Username <$> (o .: "username"))
         <*> (o .: "color")
  parseJSON _ = mempty
