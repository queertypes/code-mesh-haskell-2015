module API.Errors (
  APIError(..),
  ViewErrors(..),

  unknown,
  userExists,
  unauthorized,
  badRequest,
  notFound
) where

import Data.Aeson hiding (json)
import Data.Aeson.Types
import Data.Text

import Web.Spock.Shared
import Network.HTTP.Types

data APIError
  = Unknown Text
  | AuthenticationDown
  | Unauthorized
  | BadCredentials
  | UserExists
  | DBError Text
  | BadRequest Text
  | NotFound Text

newtype ViewErrors = ViewErrors [APIError]

(.=.) :: Text -> Text -> Pair
k .=. v = k .= v

instance ToJSON APIError where
  toJSON (Unknown t) =
      object [ "type" .=. "unknown"
             , "description" .= t
             ]
  toJSON AuthenticationDown =
    object [ "type" .=. "authentication_down" ]
  toJSON Unauthorized =
    object [ "type" .=. "unauthorized" ]
  toJSON BadCredentials =
    object [ "type" .=. "incorrect_credentials" ]
  toJSON (DBError t) =
    object [ "type" .=. "database_error"
           , "description" .= t
           ]
  toJSON UserExists =
    object [ "type" .=. "user_exists"
           , "description" .=. "This email is already registered."
           ]
  toJSON (BadRequest desc) =
    object [ "type" .=. "bad_request"
           , "description" .= desc
           ]
  toJSON (NotFound msg) =
    object [ "type" .=. "not_found"
           , "description" .=. msg
           ]

instance ToJSON ViewErrors where
  toJSON (ViewErrors es) =
    object [ "errors" .= toJSON es ]

unknown :: Text -> ActionT IO ()
unknown msg = setStatus status400 >> json (Unknown msg)

unauthorized :: ActionT IO ()
unauthorized = setStatus status401 >> json Unauthorized

userExists :: ActionT IO ()
userExists = setStatus status400 >> json UserExists

badRequest :: Text -> ActionT IO ()
badRequest msg = setStatus status400 >> json (BadRequest msg)

notFound :: Text -> ActionT IO ()
notFound msg = setStatus status404 >> json (NotFound msg)
