module API.Controllers.Users where

import Prelude hiding (log)

import Control.Concurrent.STM (atomically)
import Control.Concurrent.STM.TVar
import Control.Monad.IO.Class (liftIO)
import Data.ByteString (ByteString)
import Data.Monoid
import Data.Text (Text)
import Data.UUID.V4
import Web.Spock.Shared
import qualified Data.Map as M
import qualified Data.ByteString.Base64 as B64
import qualified Data.Text as T
import qualified Data.Text.Encoding as T

import API.State
import API.Models.Common
import API.Models.User
import API.Errors
import API.Logging

echoUser :: ActionT IO ()
echoUser = do
  b <- jsonBody
  let _ = b :: Maybe User
  json b

registerUser :: Log -> TVar UserRegistry -> ActionT IO ()
registerUser log st = do
  b <- jsonBody
  let userInfo = b :: Maybe NewUser
  case userInfo of
    Nothing  -> badRequest "Failed to parse user"
    (Just (NewUser u@(Username name) p@(Pass _) color)) -> do
       lookupResult <- liftIO $ atomically $ do
         m <- readTVar st
         return $ M.lookup (u,p) m
       case lookupResult of
         (Just _) -> userExists
         Nothing  -> do
           userid <- liftIO nextRandom
           let fullUser = User userid u color
           liftIO $ atomically (modifyTVar st (\m -> M.insert (u,p) fullUser m))
           liftIO $ info log ("User " <> name <> " registered")
           json fullUser

lookup' :: TVar UserRegistry -> Username -> Pass -> IO (Maybe User)
lookup' st username' pass = do
  atomically $ do
    m <- readTVar st
    return $ M.lookup (username',pass) m

login :: TVar UserRegistry -> ActionT IO ()
login st = do
  authHeader <- header "Authentication"
  case authHeader of
    Nothing -> unauthorized
    (Just auth') -> do
       let parseResult = parseAuthHeader auth'
       case parseResult of
         Nothing -> badRequest "Auth header parsing failed"
         (Just (u, p)) -> do
           lookupResult <- liftIO $ lookup' st u p
           case lookupResult of
             Nothing -> notFound "TODO"
             (Just user') -> json user'

  where parseAuthHeader :: Text -> Maybe (Username,Pass)
        parseAuthHeader auth' =
          case T.words auth' of
            ["Basic", loginFields] ->
              case parseLogin (T.encodeUtf8 loginFields) of
                [user,pass] -> Just (Username user, Pass pass)
                _           -> Nothing
            _                      -> Nothing
        parseLogin :: ByteString -> [Text]
        parseLogin = T.splitOn ":" . T.decodeUtf8 . B64.decodeLenient

-- Basic base64(user:pass)
