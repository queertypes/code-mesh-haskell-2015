module API.Server (
  run
) where

import Prelude hiding (log)

import Control.Concurrent.STM.TVar
import Web.Spock.Safe
import API.Logging
import API.State
import API.Routes
import qualified Data.Map as M

run :: IO ()
run = do
  registry <- newTVarIO M.empty
  log <- mkLog
  runSpock 8000 $ spockT id (routes (State registry log))
