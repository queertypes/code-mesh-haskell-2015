# Code Mesh: Start

This tutorial section covers:

* Cabal
* Stack
* Project initialization
* Adding a dependency
* Creating a library
* Creating an executable depending on that library

## Steps

1. View the cabal file: codemesh-start.cabal
2. Run `stack setup` (if you don't have a Haskell compiler)
3. Run `stack init`
4. View the source files: src/Color.hs, src/Main.hs
5. Run `stack build`
6. Run `stack repl`
7. Import the `Color` module: `import Lib`
8. Show some colors: `R`, `G`, `B`
9. Import the `Main` module: `import Main`
10. Evaluate `main`
