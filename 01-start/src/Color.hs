module Color where

data Color = R | G | B
  deriving Show
