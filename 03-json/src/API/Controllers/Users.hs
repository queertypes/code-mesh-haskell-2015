module API.Controllers.Users where

import Web.Spock.Shared

import API.Models.User

echoUser :: ActionT IO ()
echoUser = do
  b <- jsonBody
  let _ = b :: Maybe User
  json b
