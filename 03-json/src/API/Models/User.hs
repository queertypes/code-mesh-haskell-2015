module API.Models.User where

import Data.Aeson
import Data.UUID
import Data.Text

--------------------------------------------------------------------------------
                           -- Data Model --
--------------------------------------------------------------------------------
data User
  = User { pid :: UUID
         , username :: Text
         , faveColor :: Color
         , email :: Maybe Text
         } deriving (Eq)

data Color
  = Red
  | Green
  | Blue
    deriving (Eq)

--------------------------------------------------------------------------------
                         -- JSON Handling --
--------------------------------------------------------------------------------
instance ToJSON Color where
  toJSON Red   = "red"
  toJSON Green = "green"
  toJSON Blue  = "blue"

instance FromJSON Color where
  parseJSON (String "red")   = pure Red
  parseJSON (String "blue")  = pure Blue
  parseJSON (String "green") = pure Green
  parseJSON _ = mempty

instance ToJSON User where
  toJSON (User p u c email') =
    object [ "id" .= toText p
           , "username" .= toJSON u
           , "color" .= toJSON c
           , "email" .= toJSON email'
           ]

instance FromJSON User where
  parseJSON (Object o) =
    User <$> (o .: "id" >>= (maybe mempty pure . fromText))
         <*> (o .: "username")
         <*> (o .: "color")
         <*> (o .:? "email")
  parseJSON _ = mempty
