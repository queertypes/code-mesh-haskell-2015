module API.Routes (
  routes
) where

import API.Controllers.Users

import Web.Spock.Safe

routes :: SpockT IO ()
routes = do
  get "hello" (text "hi there!")
  post "users" echoUser
