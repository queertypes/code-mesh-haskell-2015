module API.Server (
  run
) where

import Web.Spock.Safe
import API.Routes

run :: IO ()
run = runSpock 8000 $ spockT id routes
