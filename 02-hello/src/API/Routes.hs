module API.Routes (
  routes
) where

import Web.Spock.Safe

routes :: SpockT IO ()
routes = do
  get ("hello" <//> "you") (text "hi there, all of you!")
