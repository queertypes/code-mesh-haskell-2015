# Code Mesh: Start

This tutorial section covers:

* API Routes
* Running a simple server
* Project structure
* Adding dependencies

## Steps

1. Declare routes: src/API/Routes.hs
2. Run a server: src/API/Server.hs
3. Add dependencies in cabal: text, Spock
4. Compile: `stack build`
5. Execute: `stack exec Server
6. Repeat from the REPL: `stack repl`
7. Import main: `import Main`
8. Evaluate main: `main`
9. Interact with the server: `curl localhost:8000/hello/you`
