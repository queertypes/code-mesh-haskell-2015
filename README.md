# Code Mesh 2015: Haskell Web Services Tutorial

This collection of materials covers parts of what it takes to create a
Haskell web service.

Lots of code, lots of repetition, and plenty of
[Stack](http://docs.haskellstack.org/en/stable/README.html).
